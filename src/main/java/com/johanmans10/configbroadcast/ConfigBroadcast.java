package com.johanmans10.configbroadcast;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.List;
import java.util.Random;

public class ConfigBroadcast extends JavaPlugin {

    @Override
    public void onEnable() {
        saveDefaultConfig();
        long delay = getConfig().getLong("broadast-delay", 900);
        Bukkit.getScheduler().scheduleSyncRepeatingTask(this, this::sendBroadcast, 200, delay * 20);
    }

    private void sendBroadcast() {
        reloadConfig();

        List<String> messages = getConfig().getStringList("messages");
        if (messages.isEmpty()) {
            this.getLogger().warning("There are no valid messages in the config!");
            return;
        }

        int rand = new Random().nextInt(messages.size());
        this.getLogger().info("Max number: " + messages.size());
        this.getLogger().info("Random number: " + rand);
        try {
            String message = messages.get(rand);
            message = ChatColor.translateAlternateColorCodes('&', message);
            Bukkit.broadcastMessage(message);
        } catch (Exception e) {
            this.getLogger().severe("Error getting message");
            e.printStackTrace();
        }

    }
}
